package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();

    @Test
    @TestCaseName("Run testAddTask for addTask(session, random, random)")
    public void testAddTask() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);

        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(
                open, title, description
        );
        Assert.assertNotNull(addProject);
        Assert.assertEquals(title, addProject.getTitle());
        Assert.assertEquals(description, addProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex(session, 0, random, random)")
    public void testUpdateTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProject =
                this.projectEndpoint.updateProjectByIndex(open, 0, title, description);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(title, updateProject.getTitle());
        Assert.assertEquals(description, updateProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById(session, id, random, random)")
    public void testUpdateTaskById( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final ProjectDTO getProject = this.projectEndpoint.getProjectByIndex(open, 0);
        Assert.assertNotNull(getProject);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final ProjectDTO updateProject = this.projectEndpoint.updateProjectById(
                open, getProject.getId(), title, description
        );
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(getProject.getId(), updateProject.getId());
        Assert.assertEquals(getProject.getUserId(), updateProject.getUserId());
        Assert.assertEquals(title, updateProject.getTitle());
        Assert.assertEquals(description, updateProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById(session, id)")
    public void testDeleteTaskById() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectById(open, addProject.getId());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(addProject.getId(), deleteProject.getId());
        Assert.assertEquals(addProject.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), deleteProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex(session, 0)")
    public void testDeleteTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);
        @NotNull final ProjectDTO getTask = this.projectEndpoint.getProjectByIndex(open, 0);
        Assert.assertNotNull(getTask);

        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectByIndex(open, 0);
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(getTask.getId(), deleteProject.getId());
        Assert.assertEquals(getTask.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(getTask.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(getTask.getDescription(), deleteProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle(session, title))")
    public void testDeleteTaskByTitle() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO deleteProject = this.projectEndpoint.deleteProjectByTitle(open, addProject.getTitle());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(addProject.getId(), deleteProject.getId());
        Assert.assertEquals(addProject.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), deleteProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks(session)")
    public void testDeleteAllTasks() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final Collection<ProjectDTO> deleteProject = this.projectEndpoint.deleteAllProjects(open);
        Assert.assertNotNull(deleteProject);
        Assert.assertNotEquals(0, deleteProject.size());
        final boolean isUserTasks = deleteProject.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById(session, id))")
    public void testGetTaskById() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO getProject = this.projectEndpoint.getProjectById(open, addProject.getId());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProject.getId(), getProject.getId());
        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex(session, 0)")
    public void testGetTaskByIndex() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final ProjectDTO getProject = this.projectEndpoint.getProjectByIndex(open, 0);
        Assert.assertNotNull(getProject);
        Assert.assertEquals(open.getUserId(), getProject.getUserId());
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle(session, title)")
    public void testGetTaskByTitle() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final ProjectDTO addProject = this.projectEndpoint.addProject(open, title, description);
        Assert.assertNotNull(addProject);

        @Nullable final ProjectDTO getProject = this.projectEndpoint.getProjectByTitle(open, addProject.getTitle());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProject.getId(), getProject.getId());
        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks(session)")
    public void testGetAllTasks() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.projectEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<ProjectDTO> getAllProject = this.projectEndpoint.getAllProjects(open);
        Assert.assertNotNull(getAllProject);
        Assert.assertNotEquals(0, getAllProject.size());
        final boolean isUserProjects = getAllProject.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserProjects);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

}