
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteProjectByTitleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteProjectByTitleResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedProject" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}projectDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteProjectByTitleResponse", propOrder = {
    "deletedProject"
})
public class DeleteProjectByTitleResponse {

    protected ProjectDTO deletedProject;

    /**
     * Gets the value of the deletedProject property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectDTO }
     *     
     */
    public ProjectDTO getDeletedProject() {
        return deletedProject;
    }

    /**
     * Sets the value of the deletedProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectDTO }
     *     
     */
    public void setDeletedProject(ProjectDTO value) {
        this.deletedProject = value;
    }

}
