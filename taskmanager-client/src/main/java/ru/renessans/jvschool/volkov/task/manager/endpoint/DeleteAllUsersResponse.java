
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteAllUsersResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteAllUsersResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteAllUsersResponse", propOrder = {
    "deletedUsers"
})
public class DeleteAllUsersResponse {

    protected boolean deletedUsers;

    /**
     * Gets the value of the deletedUsers property.
     * 
     */
    public boolean isDeletedUsers() {
        return deletedUsers;
    }

    /**
     * Sets the value of the deletedUsers property.
     * 
     */
    public void setDeletedUsers(boolean value) {
        this.deletedUsers = value;
    }

}
