package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

@AllArgsConstructor
public final class EndpointLocatorService implements IEndpointLocatorService {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository;

    @NotNull
    public AuthenticationEndpoint getAuthenticationEndpoint() {
        return this.endpointLocatorRepository.getAuthenticationEndpoint();
    }

    @NotNull
    public AdminEndpoint getAdminEndpoint() {
        return this.endpointLocatorRepository.getAdminEndpoint();
    }

    @NotNull
    public AdminDataInterChangeEndpoint getAdminDataInterChangeEndpoint() {
        return this.endpointLocatorRepository.getAdminDataInterChangeEndpoint();
    }

    @NotNull
    public SessionEndpoint getSessionEndpoint() {
        return this.endpointLocatorRepository.getSessionEndpoint();
    }

    @NotNull
    public UserEndpoint getUserEndpoint() {
        return this.endpointLocatorRepository.getUserEndpoint();
    }

    @NotNull
    public ProjectEndpoint getProjectEndpoint() {
        return this.endpointLocatorRepository.getProjectEndpoint();
    }

    @NotNull
    public TaskEndpoint getTaskEndpoint() {
        return this.endpointLocatorRepository.getTaskEndpoint();
    }

}