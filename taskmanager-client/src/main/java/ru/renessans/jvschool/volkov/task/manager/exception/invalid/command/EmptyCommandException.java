package ru.renessans.jvschool.volkov.task.manager.exception.invalid.command;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyCommandException extends AbstractException {

    @NotNull
    private static final String EMPTY_COMMAND = "Ошибка! Параметр \"команда\" является пустым или null!\n";

    public EmptyCommandException() {
        super(EMPTY_COMMAND);
    }

}