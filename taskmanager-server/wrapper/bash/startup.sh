#!/usr/bin/env bash

echo "Attempting to start the task server..."

if [ -f ./taskmanager-server.pid ]; then
	echo "Task manager server already started!"
	exit 1;
fi

mkdir -p ../bash/logs
rm -f ../bash/logs/taskmanager-server.log
nohup java -jar ../../taskmanager-server.jar > ../bash/logs/taskmanager-server.log 2>&1 &
echo $! > taskmanager-server.pid
echo "Task manager server is running with pid "$!