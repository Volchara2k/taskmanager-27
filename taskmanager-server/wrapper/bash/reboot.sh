#!/usr/bin/env bash

echo "Attempting to reboot task server..."

if [ ! -f taskmanager-server.pid ]; then
	echo "Task manager server pid not found so server is not running!"
	exit 1;
fi

# shellcheck disable=SC2046
echo 'Process with pid '$(cat taskmanager-server.pid)' was killed!';
# shellcheck disable=SC2046
kill -9 $(cat taskmanager-server.pid)
rm taskmanager-server.pid

mkdir -p ../bash/logs
rm -f ../bash/logs/taskmanager-server.log
nohup java -jar ../../taskmanager-server.jar > ../bash/logs/taskmanager-server.log 2>&1 &
echo $! > taskmanager-server.pid
echo "Task manager server is running with pid "$!