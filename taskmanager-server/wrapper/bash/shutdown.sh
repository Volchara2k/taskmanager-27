#!/usr/bin/env bash

echo "Attempting to shutdown the task server..."

if [ ! -f taskmanager-server.pid ]; then
	echo "Task manager server pid not found!"
	exit 1;
fi

# shellcheck disable=SC2046
echo 'Process with pid '$(cat ./taskmanager-server.pid)' was killed!';
# shellcheck disable=SC2046
kill -9 $(cat ./taskmanager-server.pid)
rm ./taskmanager-server.pid
echo "It's okay!"