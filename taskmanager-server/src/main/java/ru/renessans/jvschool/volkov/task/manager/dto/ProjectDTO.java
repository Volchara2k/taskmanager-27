package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String title = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    @NotNull
    private Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    @Override
    public String toString() {
        return "Заголовок проекта: " + this.getTitle() +
                ", описание проекта: " + this.getDescription() +
                "\nИдентификатор: " + super.id + "\n";
    }

}