package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidEntityManagerException extends AbstractException {

    @NotNull
    private static final String EMPTY_MANAGER = "Ошибка! Параметр \"менеджер управления сущностями\" является null!\n";

    public InvalidEntityManagerException() {
        super(EMPTY_MANAGER);
    }

}