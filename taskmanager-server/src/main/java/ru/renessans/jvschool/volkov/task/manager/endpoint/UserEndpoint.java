package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IUserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull final IServiceLocatorService service
    ) {
        super(service);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.getUserById(current.getUserId());

        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserDTO editProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.editProfileById(current.getUserId(), firstName);

        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserDTO editProfileWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.editProfileById(current.getUserId(), firstName, lastName);

        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserDTO updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        @Nullable final User user = userService.updatePasswordById(current.getUserId(), newPassword);

        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

}