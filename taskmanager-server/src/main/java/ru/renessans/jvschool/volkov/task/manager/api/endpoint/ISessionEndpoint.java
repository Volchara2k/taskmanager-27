package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.jws.WebMethod;

public interface ISessionEndpoint {

    @Nullable
    SessionDTO openSession(
            @Nullable String login,
            @Nullable String password
    );

    boolean closeSession(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    SessionDTO validateSession(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    @WebMethod
    SessionDTO validateSessionWithCommandRole(
            @Nullable SessionDTO sessionDTO,
            @Nullable UserRole commandRole
    );

    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable SessionDTO sessionDTO
    );

    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable SessionDTO sessionDTO,
            @Nullable UserRole commandRole
    );

}