package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class TaskUserRepository extends Repository<Task> implements ITaskUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskUserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Task persist(@NotNull final Task task) {
        @NotNull final User user = this.entityManager.find(User.class, task.getUserId());
        task.setUser(user);
        return super.persist(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task deleteByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new InvalidTaskException();
        this.entityManager.remove(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task deleteById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new InvalidTaskException();
        this.entityManager.remove(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task deleteByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Task task = getByTitle(userId, title);
        if (Objects.isNull(task)) throw new InvalidTaskException();
        this.entityManager.remove(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> deleteAll(
            @NotNull final String userId
    ) {
        @Nullable final Collection<Task> tasks = getAll(userId);
        tasks.forEach(this.entityManager::remove);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAll(
            @NotNull final String userId
    ) {
        @NotNull final String jpqlQuery = "from Task WHERE user.id = :userId ORDER BY creationDate";
        return this.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final String jpqlQuery = "SELECT e FROM Task e WHERE e.user.id = :userId ORDER BY creationDate";
        @NotNull final List<Task> tasks = this.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .getResultList();
        if (tasks.size() < index) return null;
        return tasks.get(index);
    }

    @Nullable
    @Override
    public Task getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpqlQuery = "SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id";
        @NotNull final List<Task> tasks = this.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @NotNull final String jpqlQuery = "SELECT e FROM Task e WHERE e.user.id = :userId AND e.title = :title";
        @NotNull final List<Task> tasks = this.entityManager.createQuery(jpqlQuery, Task.class)
                .setParameter("userId", userId)
                .setParameter("title", title)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @NotNull
    @Override
    public Collection<Task> getAllRecords() {
        @NotNull final String jpqlQuery = "from Task ORDER BY creationDate";
        return this.entityManager.createQuery(jpqlQuery, Task.class)
                .getResultList();
    }

}