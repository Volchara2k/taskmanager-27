package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

import java.util.Arrays;
import java.util.List;

public final class EndpointRepository implements IEndpointLocatorRepository {

    @NotNull
    private final IAuthenticationEndpoint authEndpoint = new AuthenticationEndpoint();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private final IAdminDataInterChangeEndpoint dataEndpoint = new AdminDataInterChangeEndpoint();

    @NotNull
    @Override
    public List<AbstractEndpoint> getAllEndpoints() {
        return Arrays.asList(
                getAuthEndpoint(),
                getSessionEndpoint(),
                getAdminEndpoint(),
                getUserEndpoint(),
                getTaskEndpoint(),
                getProjectEndpoint(),
                getDataEndpoint()
        );
    }

    @Nullable
    @Override
    public AbstractEndpoint getAuthEndpoint() {
        if (this.authEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.authEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getSessionEndpoint() {
        if (this.sessionEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.sessionEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getAdminEndpoint() {
        if (this.adminEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.adminEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getUserEndpoint() {
        if (this.userEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.userEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getTaskEndpoint() {
        if (this.taskEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.taskEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getProjectEndpoint() {
        if (this.projectEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.projectEndpoint;
        return null;
    }

    @Nullable
    @Override
    public AbstractEndpoint getDataEndpoint() {
        if (this.dataEndpoint instanceof AbstractEndpoint) return (AbstractEndpoint) this.dataEndpoint;
        return null;
    }

}