package ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidHashLineException extends AbstractException {

    @NotNull
    private static final String EMPTY_LINE = "Ошибка! Параметр \"строка для хеширования\" является пустым или null!\n";

    public InvalidHashLineException() {
        super(EMPTY_LINE);
    }

}