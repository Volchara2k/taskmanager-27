package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidValuesException extends AbstractException {

    @NotNull
    private static final String EMPTY_VALUES = "Ошибка! Параметр \"значения\" является null!\n";

    public InvalidValuesException() {
        super(EMPTY_VALUES);
    }

}