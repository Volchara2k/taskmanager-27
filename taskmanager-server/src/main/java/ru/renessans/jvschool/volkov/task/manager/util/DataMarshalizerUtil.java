package ru.renessans.jvschool.volkov.task.manager.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidPathnameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidKeyException;

import java.io.File;
import java.util.Objects;

@UtilityClass
public class DataMarshalizerUtil {

    @SneakyThrows
    public <T> T writeToJson(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidKeyException();
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File file = FileUtil.create(pathname);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromJson(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        if (Objects.isNull(tClass)) throw new InvalidKeyException();
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final File file = new File(pathname);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToXml(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidKeyException();
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File file = FileUtil.create(pathname);
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromXml(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        if (Objects.isNull(tClass)) throw new InvalidKeyException();
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final File file = new File(pathname);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.readValue(file, tClass);
    }

    @SneakyThrows
    public <T> T writeToYaml(
            @Nullable final T t,
            @Nullable final String pathname
    ) {
        if (Objects.isNull(t)) throw new InvalidKeyException();
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);
        @NotNull final File file = FileUtil.create(pathname);
        yamlMapper.writeValue(file, t);
        return t;
    }

    @NotNull
    @SneakyThrows
    public <T> T readFromYaml(
            @Nullable final String pathname,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(pathname)) throw new InvalidPathnameException();
        if (Objects.isNull(tClass)) throw new InvalidKeyException();
        if (FileUtil.isNotExists(pathname)) throw new InvalidFileException();
        @NotNull final File file = new File(pathname);
        @NotNull final YAMLFactory yamlFactory = new YAMLFactory();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper(yamlFactory);
        return yamlMapper.readValue(file, tClass);
    }

}