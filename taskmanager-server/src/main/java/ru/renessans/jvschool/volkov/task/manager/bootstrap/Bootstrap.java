package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointService;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import java.util.Collection;
import java.util.List;

public final class Bootstrap {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IEndpointLocatorRepository endpointRepository = new EndpointRepository();

    @NotNull
    private final EndpointService endpointService = new EndpointService(endpointRepository);

    public void run() {
        loadConfiguration();
        buildEntityManagerFactory();
        initialDemoData();
        publishWebServices();
    }

    private void loadConfiguration() {
        @NotNull final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        configService.load();
    }

    private void buildEntityManagerFactory() {
        @NotNull final IEntityManagerFactoryService connectionService = this.serviceLocator.getEntityManagerService();
        connectionService.build();
    }

    private void initialDemoData() {
        @NotNull final IUserService userService = this.serviceLocator.getUserService();
        @NotNull final Collection<User> users = userService.initialDemoData();
        @NotNull final ITaskUserService taskService = this.serviceLocator.getTaskService();
        taskService.initialDemoData(users);
        @NotNull final IProjectUserService projectService = this.serviceLocator.getProjectService();
        projectService.initialDemoData(users);
    }

    private void publishWebServices() {
        @NotNull final List<AbstractEndpoint> endpoints = this.endpointService.createEndpoints(this.serviceLocator);
        @Nullable final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        @Nullable final String host = configService.getServerHost();
        @Nullable final Integer port = configService.getServerPort();
        EndpointUtil.publish(endpoints, host, port);
    }

}