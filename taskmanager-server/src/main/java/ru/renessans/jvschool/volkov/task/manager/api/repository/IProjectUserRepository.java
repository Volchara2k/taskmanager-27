package ru.renessans.jvschool.volkov.task.manager.api.repository;

import ru.renessans.jvschool.volkov.task.manager.model.Project;

public interface IProjectUserRepository extends IOwnerUserRepository<Project> {
}