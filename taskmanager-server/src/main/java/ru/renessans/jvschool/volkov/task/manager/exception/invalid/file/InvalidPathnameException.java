package ru.renessans.jvschool.volkov.task.manager.exception.invalid.file;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPathnameException extends AbstractException {

    @NotNull
    private static final String EMPTY_PATHNAME = "Ошибка! Параметр \"путь файла\" является null!\n";

    public InvalidPathnameException() {
        super(EMPTY_PATHNAME);
    }

}