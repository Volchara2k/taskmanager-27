package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDomainProvider;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.AdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.AdapterLocatorService;

import java.util.Collection;
import java.util.stream.Collectors;

@RunWith(value = JUnitParamsRunner.class)
public final class DomainServiceTest {

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService();

    @NotNull
    private static final IEntityManagerFactoryService ENTITY_SERVICE = new EntityManagerFactoryService(CONFIG_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(ENTITY_SERVICE);

    @NotNull
    private static final ITaskUserService TASK_SERVICE = new TaskUserService(ENTITY_SERVICE);

    @NotNull
    private static final IProjectUserService PROJECT_SERVICE = new ProjectUserService(ENTITY_SERVICE);

    @NotNull
    private static final IAdapterRepository ADAPTER_REPOSITORY = new AdapterRepository();

    @NotNull
    private static final IAdapterLocatorService ADAPTER_SERVICE = new AdapterLocatorService(ADAPTER_REPOSITORY);

    @NotNull
    private static final IUserAdapterService USER_ADAPTER = ADAPTER_SERVICE.getUserAdapter();

    @NotNull
    private static final ITaskAdapterService TASK_ADAPTER = ADAPTER_SERVICE.getTaskAdapter();

    @NotNull
    private static final IProjectAdapterService PROJECT_ADAPTER = ADAPTER_REPOSITORY.getProjectAdapter();

    @NotNull
    private static final IDomainService DOMAIN_SERVICE = new DomainService(
            USER_SERVICE, TASK_SERVICE, PROJECT_SERVICE, ADAPTER_SERVICE
    );

    @BeforeClass
    public static void preparingConfigurationBefore() {
        Assert.assertNotNull(CONFIG_SERVICE);
        Assert.assertNotNull(ENTITY_SERVICE);
        CONFIG_SERVICE.load();
        ENTITY_SERVICE.build();
    }

    @BeforeClass
    public static void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(USER_SERVICE);
        Assert.assertNotNull(ADAPTER_REPOSITORY);
        Assert.assertNotNull(ADAPTER_SERVICE);
        Assert.assertNotNull(USER_ADAPTER);
        Assert.assertNotNull(TASK_ADAPTER);
        Assert.assertNotNull(PROJECT_ADAPTER);
        Assert.assertNotNull(TASK_SERVICE);
        Assert.assertNotNull(PROJECT_SERVICE);
        Assert.assertNotNull(DOMAIN_SERVICE);
    }

//    @Test
//    @TestCaseName("Run testDataImport for dataImport({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testDataImport(
//            @NotNull final DomainDTO domainDTO
//    ) {
//        Assert.assertNotNull(domainDTO);
//
//        @Nullable final DomainDTO importDomain = DOMAIN_SERVICE.dataImport(domainDTO);
//        Assert.assertNotNull(importDomain);
//        @NotNull final Collection<UserDTO> importDomainUsers = importDomain.getUsers();
//        Assert.assertNotNull(importDomainUsers);
//        Assert.assertNotEquals(0, importDomainUsers.size());
//        @NotNull final Collection<User> conversionUsers =
//                importDomainUsers.stream().map(USER_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<TaskDTO> importDomainTasks = importDomain.getTasks();
//        Assert.assertNotNull(importDomainTasks);
//        Assert.assertNotEquals(0, importDomainTasks.size());
//        @NotNull final Collection<Task> conversionTasks =
//                importDomainTasks.stream().map(TASK_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionTasks);
//        @NotNull final Collection<ProjectDTO> importDomainProjects = importDomain.getProjects();
//        Assert.assertNotNull(importDomainProjects);
//        Assert.assertNotEquals(0, importDomainProjects.size());
//        @NotNull final Collection<Project> conversionProjects =
//                importDomainProjects.stream().map(PROJECT_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionProjects);
//        Assert.assertEquals(importDomain.getUsers().toString(), USER_SERVICE.getAllRecords().toString());
//        Assert.assertEquals(importDomain.getProjects().toString(), PROJECT_SERVICE.getAllRecords().toString());
//        Assert.assertEquals(importDomain.getTasks().toString(), TASK_SERVICE.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testDataExport for dataExport(domain)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testDataExport(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(USER_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = USER_SERVICE.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(TASK_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionTasks);
//        @NotNull final Collection<Task> setAllTaskRecords = TASK_SERVICE.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(PROJECT_ADAPTER::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionProjects);
//        @NotNull final Collection<Project> setAllProjectRecords = PROJECT_SERVICE.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = new DomainDTO();
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO exportDomain = DOMAIN_SERVICE.dataExport(domain);
//        Assert.assertNotNull(exportDomain);
//        Assert.assertEquals(USER_SERVICE.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(PROJECT_SERVICE.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(TASK_SERVICE.getAllRecords().toString(), domain.getTasks().toString());
//    }

}