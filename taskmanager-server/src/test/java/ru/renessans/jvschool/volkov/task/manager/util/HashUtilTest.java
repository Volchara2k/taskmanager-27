package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataHashUtilProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidHashLineException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalHashAlgorithmException;
import ru.renessans.jvschool.volkov.task.manager.marker.InternalDangerImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

@RunWith(value = JUnitParamsRunner.class)
public final class HashUtilTest {

    @Test(expected = InvalidHashLineException.class)
    @TestCaseName("Run testNegativeGetSaltHashLineWithoutLine for getSaltHashLine(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetSaltHashLineWithoutLine(
            @Nullable final String line
    ) {
        HashUtil.getSaltHashLine(line);
    }


    @Test(expected = IllegalHashAlgorithmException.class)
    @Category(InternalDangerImplementation.class)
    @Ignore("Тест выполняется при ошибке внутренней реализации - нелегально выбранного алгоритма")
    @TestCaseName("Run testNegativeAlgorithmGetSaltHashLine for getSaltHashLine(\"{0}\")")
    @Parameters(
            source = CaseDataHashUtilProvider.class,
            method = "validSaltHashLinesCaseData"
    )
    public void testNegativeAlgorithmGetSaltHashLine(
            @NotNull final String result,
            @NotNull final String hashed
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(hashed);
        HashUtil.getSaltHashLine(hashed);
    }

    @Test(expected = InvalidHashLineException.class)
    @TestCaseName("Run testNegativeGetHashLineWithoutLine with for getHashLine(\"{0}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetHashLineWithoutLine(
            @Nullable final String line
    ) {
        HashUtil.getHashLine(line);
    }

    @Test(expected = IllegalHashAlgorithmException.class)
    @Category(InternalDangerImplementation.class)
    @Ignore("Тест выполняется при ошибке внутренней реализации - нелегально выбранного алгоритма")
    @TestCaseName("Run testNegativeAlgorithmGetHashLine with for getHashLine(\"{0}\")")
    @Parameters(
            source = CaseDataHashUtilProvider.class,
            method = "validHashLinesCaseData"
    )
    public void testNegativeAlgorithmGetHashLine(
            @NotNull final String result,
            @NotNull final String hashed
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(hashed);
        HashUtil.getHashLine(hashed);
    }

    @Test
    @TestCaseName("Run testGetSaltHashLine: \"{0}\" for getSaltHashLine(\"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataHashUtilProvider.class,
            method = "validSaltHashLinesCaseData"
    )
    public void testGetSaltHashLine(
            @NotNull final String result,
            @NotNull final String line
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(line);
        @NotNull final String saltHashLine = HashUtil.getSaltHashLine(line);
        Assert.assertNotNull(saltHashLine);
        Assert.assertEquals(result, saltHashLine);
    }

    @Test
    @TestCaseName("Run testGetHashLine: \"{0}\" for getHashLine(\"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataHashUtilProvider.class,
            method = "validHashLinesCaseData"
    )
    public void testGetHashLine(
            @NotNull final String result,
            @NotNull final String line
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(line);
        @NotNull final String hashLine = HashUtil.getHashLine(line);
        Assert.assertNotNull(hashLine);
        Assert.assertEquals(result, hashLine);
    }

}