package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataInterChangeProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidPathnameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidKeyException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.io.File;
import java.io.Serializable;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
public final class DataSerializerUtilTest {

    @Rule
    @NotNull
    public TemporaryFolder file = new TemporaryFolder();

    @Test(expected = InvalidKeyException.class)
    @TestCaseName("Run testNegativeWriteToBinWithoutObject for writeToBin(null, random)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @SneakyThrows
    public void testNegativeWriteToBinWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataSerializerUtil.writeToBin(null, filename);
    }

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeWriteToBinWithoutFilename for writeToBin({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesCaseData"
    )
    @SneakyThrows
    public void testNegativeWriteToBinWithoutFilename(
            @NotNull final Serializable object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataSerializerUtil.writeToBin(object, filename);
    }

    @Test(expected = InvalidKeyException.class)
    @TestCaseName("Run testNegativeReadFromBinWithoutObject for readFromBin(random, null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @SneakyThrows
    public void testNegativeReadFromBinWithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataSerializerUtil.readFromBin(filename, null);
    }

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeReadFromBinWithoutFilename for readFromBin({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesClassCaseData"
    )
    @SneakyThrows
    public void testNegativeReadFromBinWithoutFilename(
            @NotNull final Class<Serializable> sClass,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(sClass);
        DataSerializerUtil.readFromBase64(filename, sClass);
    }

    @Test(expected = InvalidFileException.class)
    @TestCaseName("Run testNegativeReadFromBinWithoutExistsFile for readFromBin({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testNegativeReadFromBinWithoutExistsFile(
            @NotNull final Class<Serializable> sClass,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(sClass);
        Assert.assertNotNull(filename);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        DataSerializerUtil.readFromBin(filepath, sClass);
    }

    @Test(expected = InvalidKeyException.class)
    @TestCaseName("Run testNegativeWriteToBase64WithoutObject for writeToBase64(null, random)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @SneakyThrows
    public void testNegativeWriteToBase64WithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataSerializerUtil.writeToBase64(null, filename);
    }

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeWriteToBase64WithoutFilename for writeToBase64({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesCaseData"
    )
    @SneakyThrows
    public void testNegativeWriteToBase64WithoutFilename(
            @NotNull final Serializable object,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(object);
        DataSerializerUtil.writeToBase64(object, filename);
    }

    @Test(expected = InvalidKeyException.class)
    @TestCaseName("Run testNegativeReadFromBase64WithoutObject for readFromBin(random, null)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @SneakyThrows
    public void testNegativeReadFromBase64WithoutObject() {
        @NotNull final String filename = UUID.randomUUID().toString();
        Assert.assertNotNull(filename);
        DataSerializerUtil.readFromBase64(filename, null);
    }

    @Test(expected = InvalidPathnameException.class)
    @TestCaseName("Run testNegativeReadFromBase64WithoutFilename for readFromBase64({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "invalidFilenamesClassCaseData"
    )
    @SneakyThrows
    public void testNegativeReadFromBase64WithoutFilename(
            @NotNull final Class<Serializable> sClass,
            @Nullable final String filename
    ) {
        Assert.assertNotNull(sClass);
        DataSerializerUtil.readFromBase64(filename, sClass);
    }

    @Test(expected = InvalidFileException.class)
    @TestCaseName("Run testNegativeReadFromBase64WithoutExistsFile for readFromBase64({0}, \"{1}\")")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testNegativeReadFromBase64WithoutExistsFile(
            @NotNull final Class<Serializable> sClass,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(sClass);
        Assert.assertNotNull(filename);
        @NotNull final File root = this.file.getRoot();
        Assert.assertNotNull(root);
        @NotNull final String rootPath = root.getAbsolutePath();
        Assert.assertNotNull(rootPath);
        @NotNull final String filepath = rootPath + File.separator + filename;
        Assert.assertNotNull(filepath);

        DataSerializerUtil.readFromBase64(filepath, sClass);
    }

    @Test
    @TestCaseName("Run testWriteToBin for writeToBin({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testWriteToBin(
            @NotNull final Serializable object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file + ".bin";
        Assert.assertNotNull(filepath);

        @NotNull final Serializable write = DataSerializerUtil.writeToBin(object, filepath);
        Assert.assertNotNull(write);
        final boolean cleared = FileUtil.delete(filepath);
        Assert.assertTrue(cleared);
    }

    @Test
    @TestCaseName("Run testReadFromBin for readFromXml({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testReadFromBin(
            @NotNull final Class<Serializable> sClass,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(sClass);
        Assert.assertNotNull(filename);
        Assert.assertNotNull(this.file);
        @NotNull final File file = this.file.newFile(filename);
        Assert.assertNotNull(file);
        @NotNull final String filepath = file + ".bin";
        Assert.assertNotNull(filepath);
        @NotNull final Serializable write =  DataSerializerUtil.writeToBin(filename, filepath);
        Assert.assertNotNull(write);

        @NotNull final Serializable read =  DataSerializerUtil.readFromBin(filepath, sClass);
        Assert.assertNotNull(read);
        final boolean cleared = FileUtil.delete(filepath);
        Assert.assertTrue(cleared);
    }

    @Test
    @TestCaseName("Run testWriteToBase64 for writeToBase64({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validLinesCaseData"
    )
    @SneakyThrows
    public void testWriteToBase64(
            @NotNull final Serializable object,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(filename);
        @NotNull final String filepath = filename + ".base64";
        Assert.assertNotNull(filepath);
        @Nullable final Serializable write = DataSerializerUtil.writeToBase64(object, filepath);
        Assert.assertNotNull(write);
        final boolean cleared = FileUtil.delete(filepath);
        Assert.assertTrue(cleared);
    }

    @Test
    @TestCaseName("Run testReadFromBase64 for readFromBase64({0}, \"{1}\")")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataInterChangeProvider.class,
            method = "validClassesCaseData"
    )
    @SneakyThrows
    public void testReadFromBase64(
            @NotNull final Class<Serializable> sClass,
            @NotNull final String filename
    ) {
        Assert.assertNotNull(sClass);
        Assert.assertNotNull(filename);
        @NotNull final String filepath = filename + ".base64";
        Assert.assertNotNull(filepath);
        @NotNull final Serializable write = DataSerializerUtil.writeToBase64(filename, filepath);
        Assert.assertNotNull(write);

        @NotNull final Serializable read = DataSerializerUtil.readFromBase64(filepath, sClass);
        Assert.assertNotNull(read);
        final boolean cleared = FileUtil.delete(filepath);
        Assert.assertTrue(cleared);
    }

}