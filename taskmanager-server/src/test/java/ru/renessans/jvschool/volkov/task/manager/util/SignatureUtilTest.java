package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSignatureUtilProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureCycleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureObjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureSaltException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
public final class SignatureUtilTest {

    @Test(expected = InvalidSignatureObjectException.class)
    @TestCaseName("Run testNegativeGetHashObjectSignatureWithoutObject for getHashSignature(null, random, 123)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativeGetHashObjectSignatureWithoutObject() {
        @NotNull final String salt = UUID.randomUUID().toString();
        Assert.assertNotNull(salt);
        @NotNull final Integer cycle = 123;
        Assert.assertNotNull(cycle);
        SignatureUtil.getHashSignature((@Nullable Object) null, salt, cycle);
    }

    @Test(expected = InvalidSignatureSaltException.class)
    @TestCaseName("Run testNegativeGetHashObjectSignatureWithoutSalt for getHashSignature({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "invalidSaltsCaseData"
    )
    public void testNegativeGetHashObjectSignatureWithoutSalt(
            @NotNull final Object object,
            @Nullable final String salt,
            @NotNull final Integer cycle
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(cycle);
        SignatureUtil.getHashSignature(object, salt, cycle);
    }

    @Test(expected = InvalidSignatureCycleException.class)
    @TestCaseName("Run testNegativeGetHashObjectSignatureWithoutCycle for getHashSignature({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "invalidCyclesCaseData"
    )
    public void testNegativeGetHashObjectSignatureWithoutCycle(
            @NotNull final Object object,
            @NotNull final String salt,
            @Nullable final Integer cycle
    ) {
        Assert.assertNotNull(object);
        Assert.assertNotNull(salt);
        SignatureUtil.getHashSignature(object, salt, cycle);
    }

    @Test(expected = InvalidSignatureObjectException.class)
    @TestCaseName("Run testNegativeGetHashSignatureWithoutLine for getHashSignature(\"{0}\", \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetHashSignatureWithoutLine(
            @Nullable final String line,
            @NotNull final String salt,
            @NotNull final Integer cycle
    ) {
        Assert.assertNotNull(salt);
        Assert.assertNotNull(cycle);
        SignatureUtil.getHashSignature(line, salt, cycle);
    }

    @Test(expected = InvalidSignatureSaltException.class)
    @TestCaseName("Run testNegativeGetHashSignatureWithoutSalt for getHashSignature(\"{0}\", \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "invalidSaltsCaseData"
    )
    public void testNegativeGetHashSignatureWithoutSalt(
            @NotNull final String line,
            @Nullable final String salt,
            @NotNull final Integer cycle
    ) {
        Assert.assertNotNull(line);
        Assert.assertNotNull(cycle);
        SignatureUtil.getHashSignature(line, salt, cycle);
    }

    @Test(expected = InvalidSignatureCycleException.class)
    @TestCaseName("Run testNegativeGetHashSignatureWithoutCycle for getHashSignature(\"{0}\", \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "invalidCyclesCaseData"
    )
    public void testNegativeGetHashSignatureWithoutCycle(
            @NotNull final String line,
            @NotNull final String salt,
            @Nullable final Integer cycle
    ) {
        Assert.assertNotNull(line);
        Assert.assertNotNull(salt);
        SignatureUtil.getHashSignature(line, salt, cycle);
    }

    @Test
    @TestCaseName("Run testGetHashObjectSignature: \"{0}\" for getHashSignature({1}, \"{2}\", {3})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "validHashObjectsCaseData"
    )
    public void testGetHashObjectSignature(
            @NotNull final String result,
            @NotNull final Object object,
            @NotNull final String salt,
            @NotNull final Integer cycle
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(object);
        Assert.assertNotNull(salt);
        Assert.assertNotNull(cycle);

        @NotNull final String hashLine = SignatureUtil.getHashSignature(object, salt, cycle);
        Assert.assertNotNull(hashLine);
        Assert.assertEquals(result, hashLine);
    }

    @Test
    @TestCaseName("Run testGetHashSignature: \"{0}\" for getHashSignature(\"{1}\", \"{2}\", {3})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataSignatureUtilProvider.class,
            method = "validHashLinesCaseData"
    )
    public void testGetHashSignature(
            @NotNull final String result,
            @NotNull final String line,
            @NotNull final String salt,
            @NotNull final Integer cycle
    ) {
        Assert.assertNotNull(result);
        Assert.assertNotNull(line);
        Assert.assertNotNull(salt);
        Assert.assertNotNull(cycle);

        @NotNull final String hashLine = SignatureUtil.getHashSignature(line, salt, cycle);
        Assert.assertNotNull(hashLine);
        Assert.assertEquals(result, hashLine);
    }

}