package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public final class CaseDataSessionProvider {

    @SuppressWarnings("unused")
    public Object[] validSessionsCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_TEST_LOGIN,
                                DemoDataConst.USER_TEST_PASSWORD
                        ),
                        new Session(
                                System.currentTimeMillis(),
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_DEFAULT_LOGIN,
                                DemoDataConst.USER_DEFAULT_PASSWORD,
                                DemoDataConst.USER_DEFAULT_FIRSTNAME
                        ),
                        new Session(
                                System.currentTimeMillis(),
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_ADMIN_LOGIN,
                                DemoDataConst.USER_ADMIN_PASSWORD,
                                UserRole.ADMIN
                        ),
                        new Session(
                                System.currentTimeMillis(),
                                user.getId()
                        )
                }
        };
    }

}